CC = cc
CFLAGS = -O3 -Wall -Wextra
LDFLAGS =
LIBS =

all: bot

bot:	bot.o fake.o
	$(CC) $(LDFLAGS) $(LIBS) $^ -o $@

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	@reset
	@rm -f *.o bot *~
