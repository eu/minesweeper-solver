# Minesweeper Solver
#### (snippet of bot code for a popular online game)

### Usage
```sh
# Set original field in file fake.h
make
./bot
```

### Result
```sh
Original field:
. . 1 0 0 0 1 2 2 .
. . 3 2 2 1 2 F F .
. . . . . . 2 . . .
. 2 4 F 3 2 2 2 . .
. . F 3 1 1 F 2 . .
. . F 3 1 2 1 2 . .
. . 3 2 F 2 2 2 . .
. . 1 1 1 2 F F . .

Solved field:
. . 1 0 0 0 1 2 2 E
. . 3 2 2 1 2 F F E
E E F F E F 2 E E .
E 2 4 F 3 2 2 2 E .
E E F 3 1 1 F 2 F .
. F F 3 1 2 1 2 E .
. . 3 2 F 2 2 2 E .
. . 1 1 1 2 F F E .
```
 **dot** - unopened cell; **0..8** - number of bombs around;
**F** - bomb mark flag; **E** - solved empty cell (can be opened).

