/*
 * Minesweeper Solver (bot fragment for popular online game)
 * Copyright (c) 2021 Eugene P. <pieu@mail.ru>
 *
 * The Bot finds and marks expected empty cells as "E"
 * and bombs as "F" in the array battlefield->field[].
 * It never tries to guess.
 * Perhaps later I will add a guess based on probability.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bot.h"
#include "fake.h"

/*
 * Brute force method is used when there are few
 * closed cells left to find possible empty areas
 */
#define BF_LIMIT 25 // Number of this closed cells

unsigned char **data; // Debriefing field

// Out Of Memory
//
void oom()
{
	fprintf(stderr, "Out of memory\n");
	exit(1);
}

// Fast version of __builtin_popcount();
//
static inline int pop(register unsigned x)
{
	x -= ((x >> 1) & 0x55555555);
	x =  (x & 0x33333333) + ((x >> 2) & 0x33333333);
	x =  (x + (x >> 4)) & 0x0F0F0F0F;
	x += (x >> 8);
	x += (x >> 16);
	return x & 0x0000003F;
}

static inline void clear_bit(unsigned char *d, unsigned char pos)
{
	unsigned char i, b, shift;
	unsigned char bit = 0b10000000 >> pos;

	for (i = 0; i < 32; i++) {
		if (!d[i])
			continue;
		for (b = 0b10000000, shift = 0; shift < 8; shift++, b >>= 1) {
			if (!(d[i] & b) || !((i * 8 + shift) & bit))
				continue;
			d[i] ^= b;
		}
	}
}

static inline void set_bit(unsigned char *d, unsigned char pos)
{
	unsigned char i, b, shift;
	unsigned char bit = 0b10000000 >> pos;

	for (i = 0; i < 32; i++) {
		if (!d[i])
			continue;
		for (b = 0b10000000, shift = 0; shift < 8; shift++, b >>= 1) {
			if (!(d[i] & b) || ((i * 8 + shift) & bit))
				continue;
			d[i] ^= b;
		}
	}
}

void bot_init(struct battlefield *bf)
{
	unsigned char i, ix;

	data = (unsigned char **) malloc(bf->wxh * sizeof(unsigned char *));
	if (!data)
		oom();

	for (i = 0; i < bf->wxh ; i++) {
		*(data + i) = (unsigned char *) malloc(32 * sizeof(unsigned char));
		if (!*(data + i))
			oom();
	}

	// Reset
	for (i = 0; i < bf->wxh; i++)
		memset(data[i], 255, 32 * sizeof(unsigned char));

	for (i = 1; i < bf->width - 1; i++) {
		// First col [123] = 000
		ix = bf->width + i;
		clear_bit(data[ix], 0);
		clear_bit(data[ix], 1);
		clear_bit(data[ix], 2);

		// Last col  [765] = 000
		ix = bf->width * (bf->height - 2) + i;
		clear_bit(data[ix], 6);
		clear_bit(data[ix], 5);
		clear_bit(data[ix], 4);
	}

	for (i = 1; i < bf->height - 1; i++) {
		// First row [187] = 000
		ix = bf->width * i + 1;
		clear_bit(data[ix], 0);
		clear_bit(data[ix], 7);
		clear_bit(data[ix], 6);

		// Last row [345] = 000
		ix = bf->width * (i + 1) - 2;
		clear_bit(data[ix], 2);
		clear_bit(data[ix], 3);
		clear_bit(data[ix], 4);
	}
}

void bot_destroy(struct battlefield *bf)
{
	for(unsigned char i = 0; i < bf->wxh; i++)
		free(*(data + i));
	free(data);
}

static inline void check_numbers(struct battlefield *bf)
{
	unsigned char x, y, offset, n, i, shift, b;

	for (y = 1; y < bf->height - 1; y++) {
		for (x = 1; x < bf->width - 1; x++) {
			offset = y * bf->width + x;

			// Indices of surrounding cells
			unsigned char ix[] = { 0, IX1, IX2, IX3, IX4, IX5, IX6, IX7, IX8 };
			for (i = 1; i < 9; i++)
				if (bf->field[ ix[i] ] >= '0' && bf->field[ ix[i] ] <= '9')
					clear_bit(data[offset], i - 1);
				else if (bf->field[ ix[i] ] == 'F')
					set_bit(data[offset], i - 1);

			n = bf->field[offset];
			if (n < '0' || n > '9')
				continue;
			n -= '0';

			for (i = 0; i < 32; i++) {
				if (!data[offset][i])
					continue;
				for (shift = 0, b = 0b10000000; shift < 8; shift++, b >>= 1) {
					if ((data[offset][i] & b) && pop(i * 8 + shift) != n)
						data[offset][i] ^= b;
				}
			}
		}
	}
}

static inline int arch(unsigned char ix, unsigned char s, unsigned char opt)
{
	//
	//  1 2 3	IX2		IX6		IX8		IX4
	//  8   4   src 8 1 2 3 4	8 7 6 5 4	2 1 8 7 6	2 3 4 5 6
	//  7 6 5   dst 7 8 . 4 5	1 8 . 4 3	3 2 . 6 5	1 2 . 6 7
	//
	const char src[4][4] = { {0b00000001, 0b10000000, 0b00100000, 0b00010000},
				 {0b00000001, 0b00000010, 0b00001000, 0b00010000},
				 {0b01000000, 0b10000000, 0b00000010, 0b00000100},
				 {0b01000000, 0b00100000, 0b00001000, 0b00000100} };

	const char dst[4][4] = { {0b00000010, 0b00000001, 0b00010000, 0b00001000},
				 {0b10000000, 0b00000001, 0b00010000, 0b00100000},
				 {0b00100000, 0b01000000, 0b00000100, 0b00001000},
				 {0b10000000, 0b01000000, 0b00000100, 0b00000010} };

	unsigned char i, b, shift, d;

	for (i = 0; i < 32; i++) {
		if (!data[ix][i])
			continue;
		for (b = 0b10000000, shift = 0; shift < 8; shift++, b >>= 1) {
			if (!(data[ix][i] & b))
				continue;

			d = i * 8 + shift;

			if (!(s & src[opt][0]) == !(d & dst[opt][0]) &&
			    !(s & src[opt][1]) == !(d & dst[opt][1]) &&
			    !(s & src[opt][2]) == !(d & dst[opt][2]) &&
			    !(s & src[opt][3]) == !(d & dst[opt][3]))
				return 1;
		}
	}
	return 0;
}

static inline int corner(unsigned char ix, unsigned char s, unsigned char opt)
{
	//
	//  1 2 3	IX1	IX3	IX5	IX7
	//  8   4   src 2 8	2 4	4 6	8 6
	//  7 6 5   dst 4 6	8 6	2 8	2 4
	//
	const char src[4][2] = { {0b01000000, 0b00000001},
				 {0b01000000, 0b00010000},
				 {0b00010000, 0b00000100},
				 {0b00000001, 0b00000100} };

	const char dst[4][2] = { {0b00010000, 0b00000100},
				 {0b00000001, 0b00000100},
				 {0b01000000, 0b00000001},
				 {0b01000000, 0b00010000} };

	unsigned char i, b, shift, d;

	for (i = 0; i < 32; i++) {
		if (!data[ix][i])
			continue;
		for (b = 0b10000000, shift = 0; shift < 8; shift++, b >>= 1) {
			if (!(data[ix][i] & b))
				continue;

			d = i * 8 + shift;

			if (!(s & src[opt][0]) == !(d & dst[opt][0]) &&
			    !(s & src[opt][1]) == !(d & dst[opt][1]))
				return 1;
		}
	}
	return 0;
}

static inline int check3bit(unsigned char ix, unsigned char s,
			    unsigned char s1, unsigned char s2, unsigned char s3,
			    unsigned char d1, unsigned char d2, unsigned char d3)
{
	unsigned char i, b, shift, d;

	for (i = 0; i < 32; i++) {
		if (!data[ix][i])
			continue;
		for (b = 0b10000000, shift = 0; shift < 8; shift++, b >>= 1) {
			if (!(data[ix][i] & b))
				continue;

			d = i * 8 + shift;

			if (!(s & s1) == !(d & d1) &&
			    !(s & s2) == !(d & d2) &&
			    !(s & s3) == !(d & d3))
				return 1;
		}
	}
	return 0;

}

static inline int check_around(struct battlefield *bf, unsigned char x, unsigned char y)
{
	unsigned char i, b, shift, n, repeat = 0;
	unsigned char offset = y * bf->width + x;

	for (i = 0; i < 32; i++) {
		if (!data[offset][i])
			continue;
		for (b = 0b10000000, shift = 0; shift < 8; shift++, b >>= 1) {
			if (!(data[offset][i] & b))
				continue;
			n = i * 8 + shift;

			// arch opts:	[0] IX2, [1] IX6, [2] IX8, [3] IX4
			// corner opts: [0] IX1, [1] IX3, [2] IX5, [3] IX7

			if (!corner(IX1, n, 0) || !arch(IX2, n, 0) ||
			    !corner(IX3, n, 1) || !arch(IX4, n, 3) ||
			    !corner(IX5, n, 2) || !arch(IX6, n, 1) ||
			    !corner(IX7, n, 3) || !arch(IX8, n, 2) ||

			    (y > 1 && !check3bit(IX2 - bf->width, n,
				       0b10000000, 0b01000000, 0b00100000,
				       0b00000010, 0b00000100, 0b00001000)) ||

			    (x < bf->width - 2 && !check3bit(IX4 + 1, n,
				       0b00100000, 0b00010000, 0b00001000,
				       0b10000000, 0b00000001, 0b00000010)) ||

			    (y < bf->height - 2 && !check3bit(IX6 + bf->width, n,
				       0b00000010, 0b00000100, 0b00001000,
				       0b10000000, 0b01000000, 0b00100000)) ||

			    (x > 1 && !check3bit(IX8 - 1, n,
				       0b10000000, 0b00000001, 0b00000010,
				       0b00100000, 0b00010000, 0b00001000))) {

				data[offset][i] ^= b;
				repeat = 1;
				continue;
			}

		}
	}
	return repeat;
}

static inline int check_inconsistencies(struct battlefield *bf)
{
	unsigned char x, y, repeat = 0;

	for (y = 1; y < bf->height - 1; y++)
		for (x = 1; x < bf->width - 1; x++)
			repeat |= check_around(bf, x, y);

	return repeat;
}

static inline int mark_safe_cells(struct battlefield *bf)
{
	unsigned char x, y, offset, marks = 0;

	for (y = 1; y < bf->height - 1; y++) {
		for (x = 1; x < bf->width - 1; x++) {
			offset = y * bf->width + x;

			unsigned char i, shift, b, bombs = 0b11111111, empty = 0b00000000;

			for (i = 0; i < 32; i++) {
				if (!data[offset][i])
					continue;
				for (shift = 0, b = 0b10000000; shift < 8; shift++, b >>= 1) {
					if (!(data[offset][i] & b))
						continue;

					empty |= i * 8 + shift;
					bombs &= i * 8 + shift;
				}
			}

			unsigned char ix[] = { 0, IX1, IX2, IX3, IX4, IX5, IX6, IX7, IX8 };
			for (i = 1; i < 9; i++) {
				if (bf->field[ ix[i] ] != '.')
					continue;

				if (bombs & 0b10000000 >> (i - 1)) {
					bf->field[ ix[i] ] = 'F';
					marks++;
				}
				if (~empty & 0b10000000 >> (i - 1)) {
					bf->field[ ix[i] ] = 'E';
					marks++;
				}
			}
		}
	}
	return marks;
}

// Brute force method
//
static int mark_safe_cells_bf(struct battlefield *bf)
{
	unsigned char x, y, offset, marks = 0, cells = 0, bombs = 0;

	for (y = 1; y < bf->height - 1; y++) {
		for (x = 1; x < bf->width - 1; x++) {
			if (FIELD(x, y) == '.')
				cells++;
			else if (FIELD(x, y) == 'F')
				bombs++;
		}
	}
	if (bombs == bf->bombs) {
		for (y = 1; y < bf->height - 1; y++) {
			for (x = 1; x < bf->width - 1; x++) {
				if (FIELD(x, y) == '.')
					FIELD(x, y) = 'E';
			}
		}
		return 1;
	}

	if (bombs > bf->bombs || cells > BF_LIMIT)
		return 0;

	bombs = bf->bombs - bombs;

	register int i, j, k;

	unsigned char *empty = (unsigned char *) malloc(sizeof (unsigned char) * cells);
	char *mask = (char *) calloc(cells, sizeof (char));
	int *p  = (int *) malloc(bombs * sizeof(int));
	int *pl = (int *) malloc(bombs * sizeof(int));
	if (!p || !pl)
		oom();

	// Init
	for (i = 0; i < bombs; i++) {
		p[i] = i;
		pl[i] = cells - (bombs - i);
	}

	// Find empty cells and write offsets in empty[]
	for (y = 1, offset = 0; y < bf->height - 1; y++)
		for (x = 1; x < bf->width - 1; x++)
			if (FIELD(x, y) == '.')
				empty[offset++] = y * bf->width + x;

	while (1) {
		// Set test bombs
		for (i = 0; i < bombs; i++)
			*(bf->field + empty[ p[i] ]) = 'F';

		// Check field
		for (y = 1, offset = 0; y < bf->height - 1; y++) {
			for (x = 1; x < bf->width - 1; x++) {
				unsigned char mines = 0;
				unsigned char n = FIELD(x, y);
				if (n < '0' || n > '8')
					continue;

				if (FIELD(x - 1, y - 1) == 'F')
					mines++;
				if (FIELD(x,     y - 1) == 'F')
					mines++;
				if (FIELD(x + 1, y - 1) == 'F')
					mines++;
				if (FIELD(x + 1, y    ) == 'F')
					mines++;
				if (FIELD(x + 1, y + 1) == 'F')
					mines++;
				if (FIELD(x,     y + 1) == 'F')
					mines++;
				if (FIELD(x - 1, y + 1) == 'F')
					mines++;
				if (FIELD(x - 1, y    ) == 'F')
					mines++;

				if (mines != n - '0')
					goto restore_field;
			}
		}
		for (i = 0; i < bombs; i++)
			mask[ p[i] ] |= 1;

restore_field:
		for (i = 0; i < cells; i++)
			*(bf->field + empty[i]) = '.';

		if (p[0] == pl[0])
			break;

		for (j = bombs - 1; j >= 0; j--) {
			if (p[j] != pl[j]) {
				k = p[j] + 1;
				for (i = j; i < bombs; i++)
					p[i] = i - j + k;
				break;
			}
		}
	}
	for (i = 0; i < cells; i++) {
		if (!mask[i]) {
			*(bf->field + empty[i]) = 'E';
			marks++;
		}
	}

	free(pl);
	free(p);
	free(mask);
	free(empty);

	printf("Found %d empty cell(s)\n", marks);
	return marks;
}

static int bot(struct battlefield *bf)
{
	if (mark_safe_cells(bf))
		return 1;

	check_numbers(bf);

	while (check_inconsistencies(bf));

	if (mark_safe_cells(bf))
		return 1;

	return mark_safe_cells_bf(bf);
}

static void field_print(struct battlefield *bf)
{
	for (unsigned char y = 1; y < bf->height - 1; y++) {
		for (unsigned char x = 1; x < bf->width - 1; x++)
			printf("%c ", FIELD(x, y));
		printf("\n");
	}
	printf("\n");
}

int main()
{
	struct battlefield *bf = fake_field_init();
	bot_init(bf);

	field_print(bf);
	bot(bf); // if (!bot) { try to guess }
	field_print(bf);

	bot_destroy(bf);
	return 0;
}
