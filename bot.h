#define FIELD(x, y) ( *(bf->field + bf->width * (y) + (x)) )

#define IX1	(offset - bf->width - 1)
#define IX2 	(offset - bf->width)
#define IX3 	(offset - bf->width + 1)
#define IX4 	(offset + 1)
#define IX5 	(offset + bf->width + 1)
#define IX6 	(offset + bf->width)
#define IX7 	(offset + bf->width - 1)
#define IX8 	(offset - 1)

struct battlefield {
	char *field;
	unsigned char width, height, wxh, bombs;
};

void oom(void);
