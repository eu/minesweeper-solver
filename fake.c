#include <stdlib.h>

#include "bot.h"
#include "fake.h"

struct battlefield *fake_field_init()
{
        struct battlefield *bf = (struct battlefield *) malloc(sizeof(struct battlefield));
	if (!bf)
		oom();

	bf->width  = FAKE_FIELD_WIDTH;
	bf->height = FAKE_FIELD_HEIGHT;
	bf->wxh = bf->width * bf->height;
	bf->bombs  = FAKE_FIELD_BOMBS;

	static char field[FAKE_FIELD_WIDTH * FAKE_FIELD_HEIGHT] = FAKE_FIELD;
	bf->field = field;

	return bf;
}

