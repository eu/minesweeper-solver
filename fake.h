#define FAKE_FIELD_WIDTH	12
#define FAKE_FIELD_HEIGHT	10
#define FAKE_FIELD_BOMBS	15

/*
 * The working area must always be surrounded by empty cells,
 * i.e. real size FAKE_FIELD_WIDTH-2 x FAKE_FIELD_HEIGHT-2.
 * This is used to avoid checking the bounds of the array :^)
 */

#define FAKE_FIELD { \
	'?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', \
	'?', '.', '.', '1', '0', '0', '0', '1', '2', '2', '.', '?', \
	'?', '.', '.', '3', '2', '2', '1', '2', 'F', 'F', '.', '?', \
	'?', '.', '.', '.', '.', '.', '.', '2', '.', '.', '.', '?', \
	'?', '.', '2', '4', 'F', '3', '2', '2', '2', '.', '.', '?', \
	'?', '.', '.', 'F', '3', '1', '1', 'F', '2', '.', '.', '?', \
	'?', '.', '.', 'F', '3', '1', '2', '1', '2', '.', '.', '?', \
	'?', '.', '.', '3', '2', 'F', '2', '2', '2', '.', '.', '?', \
	'?', '.', '.', '1', '1', '1', '2', 'F', 'F', '.', '.', '?', \
	'?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', \
}

struct battlefield *fake_field_init(void);
